package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    @Override
    public String run(String[] args) {
        String concatArgInArray = "";
        for (String arg : args){
            concatArgInArray += arg + " ";
        }
        return concatArgInArray;
    }

    @Override
    public String getName() {
        return "echo";
    }
    
}
